﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attacker))]
public class Lizard : MonoBehaviour{

	private Animator lizardAnimator;
	private Attacker attacker;

	// Use this for initialization
	void Start(){
		lizardAnimator = GetComponent<Animator>();
		attacker = GetComponent<Attacker>();
	}

	void OnTriggerEnter2D(Collider2D collider){
		//Debug.Log("Lizard collided with: " + collider);
		GameObject obj = collider.gameObject;

		if(!obj.GetComponent<Defender>()){
			return;
		}

		lizardAnimator.SetBool("isAttacking", true);
		attacker.Attack(obj);


	}
}
