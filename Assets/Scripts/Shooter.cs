﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour{

	public GameObject projectileType;
	public GameObject gun;
	private GameObject projectiles;
	private Animator animator;
	private AttackerSpawner myLineSpawner;

	// Use this for initialization
	void Start(){
		myLineSpawner = GetMyLineSpawner();
		if(!myLineSpawner){
			Debug.LogError("Can not find AttackerSpawner in line");
		}
		animator = this.GetComponent<Animator>();
		projectiles = GameObject.Find("Projectiles");
		if(!projectiles){
			projectiles = new GameObject("Projectiles");
		}
	}
	
	// Update is called once per frame
	void Update(){
		if(AttackerAhead()){
			animator.SetBool("isAttacking", true);
		} else{
			animator.SetBool("isAttacking", false);
		}
	}

	public void FireGun(){
		GameObject projectile = Instantiate(projectileType) as GameObject;
		projectile.transform.parent = projectiles.transform;
		projectile.transform.position = gun.transform.position;
	}

	private bool AttackerAhead(){
		if(myLineSpawner.transform.childCount <= 0){
			return false;
		}
		foreach(Transform child in myLineSpawner.transform){
			if(child.position.x > this.transform.position.x){
				return true;
			}
		}
		return false;
	}

	private AttackerSpawner GetMyLineSpawner(){
		AttackerSpawner[] spawnerArray = GameObject.FindObjectsOfType<AttackerSpawner>();
		foreach(AttackerSpawner spawner in spawnerArray){
			if(spawner.transform.position.y == this.transform.position.y){
				return spawner;
			}
		}
		return null;
	}

}
