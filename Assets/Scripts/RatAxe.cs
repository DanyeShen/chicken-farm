﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attacker))]
public class RatAxe : MonoBehaviour{

	private Animator ratAnimator;
	private Attacker attacker;

	// Use this for initialization
	void Start(){
		ratAnimator = GetComponent<Animator>();
		attacker = GetComponent<Attacker>();
	}

	void OnTriggerEnter2D(Collider2D collider){
		GameObject obj = collider.gameObject;
		if(!obj.GetComponent<Defender>()){
			return;
		}
		ratAnimator.SetBool("isAttacking", true);
		attacker.Attack(obj);
	}
}