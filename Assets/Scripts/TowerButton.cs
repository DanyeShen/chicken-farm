﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerButton : MonoBehaviour{

	public GameObject towerPrefeb;
	public static GameObject selectedTower;

	private TowerButton[] buttons;
	public bool isSelected = false;



	// Use this for initialization
	void Start(){
		buttons = GameObject.FindObjectsOfType<TowerButton>();
	}
	
	// Update is called once per frame
	void Update(){
		
	}

	void OnMouseDown(){
		if(isSelected){
			selectedTower = null;
			foreach(TowerButton button in buttons){
				button.GetComponent<SpriteRenderer>().color = Color.black;
				button.isSelected = false;
			}
		} else{
			selectedTower = towerPrefeb;
			foreach(TowerButton button in buttons){
				button.GetComponent<SpriteRenderer>().color = Color.black;
				button.isSelected = false;
			}
			this.GetComponent<SpriteRenderer>().color = Color.white;
			isSelected = true;
		}

	}

}
