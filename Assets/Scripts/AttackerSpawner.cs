﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerSpawner : MonoBehaviour {

	public GameObject[] attackerPrefebs;
	private GameObject attackers;

	// Use this for initialization
	void Start () {
		attackers = GameObject.Find("Attackers");
		if(!attackers){
			attackers = new GameObject("Attackers");
		}
	}
	
	// Update is called once per frame
	void Update () {
		foreach(GameObject attacker in attackerPrefebs){
			if(IsTimeToSpawn(attacker)){
				SpawnHere(attacker);
			}
		}
	}

	private bool IsTimeToSpawn(GameObject attacker){
		float temp = attacker.GetComponent<Attacker>().spawnRate*5; // there are 5 lines
		if(Time.deltaTime>temp){
			Debug.Log("spawnRate capped by framerate");
		}
//		if((1/temp)*Time.deltaTime>=Random.value){
//		return true;
//		}
//		return false;
		return (1/temp)*Time.deltaTime>=Random.value;
	}

	private void SpawnHere(GameObject attacker){
		GameObject spawnedAttacker = Instantiate(attacker,this.transform);
		Vector3 spawnOffset = new Vector3(0,Random.Range(-0.25f,0.25f),0f);
		spawnedAttacker.transform.position = (this.transform.position + spawnOffset);
	}
}
