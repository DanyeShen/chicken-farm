﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public float speed,damage;
	public GameObject sparkPrefeb;
	private GameObject sparks;

	// Use this for initialization
	void Start () {
		sparks = GameObject.Find("Sparks");
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.right*speed*Time.deltaTime);
	}

	void OnTriggerEnter2D(Collider2D collider){
		if(!collider.gameObject.GetComponent<Attacker>()){
		return;
		}
		collider.gameObject.GetComponent<Health>().DealDamage(damage);
		GameObject spark = Instantiate(sparkPrefeb,sparks.transform);
		spark.transform.position = this.gameObject.transform.position;
		Destroy(gameObject);
	}
}
