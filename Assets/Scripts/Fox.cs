﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Attacker))]
public class Fox : MonoBehaviour{

	private Animator foxAnimator;
	private Attacker attacker;

	// Use this for initialization
	void Start(){
		foxAnimator = this.GetComponent<Animator>();
		attacker = this.GetComponent<Attacker>();
	}
	
	// Update is called once per frame
	void Update(){
		
	}

	void OnTriggerEnter2D(Collider2D collider){
		//Debug.Log("Fox collided with: " + collider);
		GameObject obj = collider.gameObject;

		if(!obj.GetComponent<Defender>()){
			return;
		}

		if(obj.GetComponent<GraveStone>()){
			foxAnimator.SetTrigger("jumpTrigger");
		} else{
			foxAnimator.SetBool("isAttacking", true);
			attacker.Attack(obj);
		}
	}
}
