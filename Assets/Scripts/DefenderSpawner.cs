﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour{

	private Camera myCamera;
	private GameObject defenders;
	private StarCollect starCollect;

	// Use this for initialization
	void Start(){
		myCamera = Camera.main;
		starCollect = GameObject.FindObjectOfType<StarCollect>();
		defenders = GameObject.Find("Defenders");
		if(!defenders){
			defenders = new GameObject("Defenders");
		}
	}
	
	// Update is called once per frame
	void Update(){
		
	}

	void OnMouseDown(){
//		Debug.Log(Input.mousePosition);
//		Debug.Log(PixelToWorldUnit(Input.mousePosition));
//		Debug.Log(SnapToGrid(PixelToWorldUnit(Input.mousePosition)));
		if(TowerButton.selectedTower){
			int cost = TowerButton.selectedTower.GetComponent<Defender>().cost;
			if(starCollect.UseStars(cost) == StarCollect.Status.SUCCESS){
				BuildTower();
			} else{
				Debug.Log("Not Enough Star");
			}
		}
	}

	Vector2 PixelToWorldUnit(Vector3 mousePixel){
		//Camera camera = Camera.main;
		Vector3 mouseWorldUnit = myCamera.ScreenToWorldPoint(mousePixel);
		return mouseWorldUnit;
	}

	Vector2 SnapToGrid(Vector2 snap){
		Vector2 grid = new Vector2(0f, 0f);
		grid.x = Mathf.Floor(snap.x) + 0.5f;
		grid.y = Mathf.Floor(snap.y) + 0.5f;
		return grid;
	}

	private void BuildTower(){
		GameObject newDefender = Instantiate(TowerButton.selectedTower, defenders.transform);
		Vector2 position = SnapToGrid(PixelToWorldUnit(Input.mousePosition));
		newDefender.transform.position = position;
		
	}
}
