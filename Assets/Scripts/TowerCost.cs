﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerCost : MonoBehaviour {

	private Text text;
	private GameObject towerPrefeb;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		towerPrefeb = gameObject.GetComponentInParent<TowerButton>().towerPrefeb;
		UpdateCost();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void UpdateCost(){
		text.text = towerPrefeb.GetComponent<Defender>().cost.ToString();
	}
}
