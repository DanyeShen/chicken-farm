﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseLine : MonoBehaviour {

	LevelManager levelManager;

	void Start(){
		levelManager = GameObject.FindObjectOfType<LevelManager>();
	}

	void OnTriggerEnter2D(Collider2D collider){
		if(collider.GetComponent<Attacker>()){
			Debug.Log("YOU LOSE");
			levelManager.LoadLose();
		}
		Destroy(collider.gameObject);
	}

}
