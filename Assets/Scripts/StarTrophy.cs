﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarTrophy : MonoBehaviour {

	private StarCollect starCollect;

	// Use this for initialization
	void Start () {
		starCollect = GameObject.Find("StarCollect").GetComponent<StarCollect>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddStar(int star){
		starCollect.AddStars(star);
	}
}
