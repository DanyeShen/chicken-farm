﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour{

	public Slider volumeSlider;
	public Slider difficultySlider;
	public LevelManager levelManager;
	private MusicManager musicManager;

	// Use this for initialization
	void Start(){
		musicManager = GameObject.FindObjectOfType<MusicManager>();
		//Debug.Log(PlayerPrefsManager.GetMasterVolume());
		//Debug.Log(PlayerPrefsManager.GetDifficulty());
		volumeSlider.value = PlayerPrefsManager.GetMasterVolume();
		difficultySlider.value = PlayerPrefsManager.GetDifficulty();
	}
	
	// Update is called once per frame
	void Update(){
		musicManager.ChangeVolume(volumeSlider.value);
	}

	public void SaveExit(){
		PlayerPrefsManager.SetMasterVolume(volumeSlider.value);
		//Debug.Log(PlayerPrefsManager.GetMasterVolume());
		PlayerPrefsManager.SetDifficulty(difficultySlider.value);
		levelManager.LoadStart();
	}

	public void SetDefault(){
		volumeSlider.value = 0.7f;
		difficultySlider.value = 2f;
	}
}
