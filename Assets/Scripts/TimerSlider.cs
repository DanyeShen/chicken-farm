﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerSlider : MonoBehaviour {

	public float levelSeconds;
	private AudioSource winAudio;
	private LevelManager levelManager;
	private Slider thisSlider;
	private bool hasWon=false;
	private GameObject winButton;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		winButton = GameObject.Find("WinButton");
		winButton.SetActive(false);
		thisSlider = this.GetComponent<Slider>();
		winAudio = this.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeSinceLevelLoad>=levelSeconds&&hasWon==false){
			winButton.SetActive(true);
			winAudio.Play();
			GameObject.Find("LoseLine").SetActive(false);
			hasWon=true;
			Invoke("WinLoad",10f);

		}
		thisSlider.value = Time.timeSinceLevelLoad/levelSeconds;
	}

	//For test only
	private void WinLoad(){
		levelManager.LoadNextLevel();
	}
}
