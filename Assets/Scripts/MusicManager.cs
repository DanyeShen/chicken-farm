﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour{

	public AudioClip[] musicList;
	private AudioSource audioSource;

	void Awake(){
		DontDestroyOnLoad(gameObject);
		//Debug.Log("DontDestroyOnLoad" + name);
	}

	// Use this for initialization
	void Start(){
		//Debug.Log("Music.Start()");
		audioSource = GetComponent<AudioSource>();
		if(Application.loadedLevel==0)
		LoadMusic(0);
	}

	void OnLevelWasLoaded(int level){
		//Debug.Log("OnLevelWasLoaded");
		LoadMusic(level);
	}

	void LoadMusic(int level){
		if(musicList[level]){
			AudioClip thisLevelMusic = musicList[level];
			//Debug.Log("thisLevelMusic");
			audioSource.clip = thisLevelMusic;
			audioSource.loop = true;
			audioSource.Play();
		}
	}

	public void ChangeVolume(float volume){
		GetComponent<AudioSource>().volume = volume;
	}
}
