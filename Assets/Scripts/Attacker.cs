﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour{
	[Tooltip("Average number in seconds between spawn")]
	public float spawnRate;
	[Range(-1f, 1.5f)] public float currentSpeed;
	private GameObject currentTarget;
	private Animator animator;

	// Use this for initialization
	void Start(){
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update(){
		transform.Translate(Vector3.left * currentSpeed * Time.deltaTime);
		if(!currentTarget){
			animator.SetBool("isAttacking", false);
		}
	}

	public void ChangeSpeed(float speed){
		currentSpeed = speed;
	}

	//Called from the animator at time of actual attack
	public void StrikeCurrentTarget(float damage){
		if(currentTarget){
			currentTarget.GetComponent<Health>().DealDamage(damage);
			if(currentTarget.GetComponent<GraveStone>()){
				currentTarget.GetComponent<Animator>().SetTrigger("isAttacked");
			}
		}
	}

	public void Attack(GameObject target){
		currentTarget = target;
	}
}
