﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public float autoLoad;

	// Use this for initialization
	void Start () {
		if(Application.loadedLevel==0)
		Invoke("LoadNextLevel",autoLoad);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadNextLevel(){
		//Debug.Log("LoadNextLevel");
		SceneManager.LoadScene(Application.loadedLevel+1);
	}

	public void Quit(){
		//Debug.Log("Quit");
		Application.Quit();
	}

	public void LoadStart(){
		//Debug.Log("To Start Scene");
		SceneManager.LoadScene("Start");
	}

	public void LoadOptions(){
		//Debug.Log("To Options Scene");
		SceneManager.LoadScene("Options");
	}

	public void LoadLevel01(){
		//Debug.Log("To Level01 Scene");
		SceneManager.LoadScene("Level01");
	}

	public void LoadWin(){
		Debug.Log("To Win Scene");
		SceneManager.LoadScene("Win");
	}

	public void LoadLose(){
		Debug.Log("To Lose Scene");
		SceneManager.LoadScene("Lose");
	}
}
