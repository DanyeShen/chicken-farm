﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarCollect : MonoBehaviour{

	public int starCollect = 100;
	public enum Status{SUCCESS,FAILURE};
	private Text starText;

	// Use this for initialization
	void Start(){
		starText = GetComponent<Text>();
		UpdateDisplay();
	}
	
	// Update is called once per frame
	void UpdateDisplay(){
		starText.text = starCollect.ToString();
	}

	public void AddStars(int number){
		starCollect += number;
		UpdateDisplay();
	}

	public Status UseStars(int number){
		if(starCollect>=number){
			starCollect -= number;
			UpdateDisplay();
			return Status.SUCCESS;
		}else{
			return Status.FAILURE;
		}

	}
}
