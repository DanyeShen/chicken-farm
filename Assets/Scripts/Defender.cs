﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defender : MonoBehaviour {

	[Tooltip("The cost in stars of this defender")]
	public int cost;
	public int value;
	private StarCollect starCollect;

	// Use this for initialization
	void Start () {
		starCollect = GameObject.Find("StarCollect").GetComponent<StarCollect>();
	}

	public void Harvest(){
		starCollect.AddStars(value);
	}
	
}
